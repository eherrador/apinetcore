using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace apinetcore.Controllers
{
    [ApiVersion( "1.0" )]
    [Route( "api/v{version:apiVersion}/[controller]" )]
    public class PingController : Controller
    {
        [HttpGet]
        [Route("ping")]
        public string Ping()
        {
            return "Pong Pong Pong";
        }

        [Authorize]
        [HttpGet("claims")]
        public object Claims()
        {
            var listClaims = User.Claims.Select(c => new { Type = c.Type, Value = c.Value });
            string fbId;

            foreach(var element in listClaims)
            {
                if (element.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier")
                    fbId = element.Value;
            }

            return listClaims;
            /*return User.Claims.Select(c =>
            new
            {
                Type = c.Type,
                Value = c.Value
            });*/
        }

        [Authorize]
        [HttpGet]
        [Route("ping/secure")]
        public string PingSecured()
        {
            return "Ya chingamos!. You only get this message if you are authenticated.";
        }
    }
}